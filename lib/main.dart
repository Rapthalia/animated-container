import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:vector_math/vector_math.dart' show radians;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Widget Animated container',
      home: AnimatedContainerExample(),
    );
  }
}

class AnimatedContainerExample extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AnimatedContainerExampleState();
  }
}

class _AnimatedContainerExampleState extends State<AnimatedContainerExample> {
  bool _isInitialValue = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Widget Animated container'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedContainer(
              duration: const Duration(seconds: 2),
              color: _isInitialValue ? Colors.blue : Colors.red,
              width: _isInitialValue ? 150 : 50,
              height: _isInitialValue ? 100 : 50,
              alignment:
                  _isInitialValue ? Alignment(-1.0, -1.0) : Alignment(1.0, 1.0),
              padding: EdgeInsets.all(_isInitialValue ? 0 : 20),
              foregroundDecoration: BoxDecoration(
                color: _isInitialValue ? Colors.blue : Colors.red,
                border: Border.all(width: _isInitialValue ? 0 : 10),
              ),
              constraints: BoxConstraints(
                maxWidth: _isInitialValue ? 100 : 50,
                maxHeight: _isInitialValue ? 100 : 50,
              ),
              transform: _isInitialValue
                  ? Matrix4.identity()
                  : Matrix4.rotationX(radians(45)),
              curve: Curves.bounceInOut,
              child: const SizedBox(width: 100, height: 100),
              onEnd: () {
                print('Animation ends');
              },
            ),
            OutlinedButton(
              child: const Text('Change value'),
              onPressed: () {
                setState(() {
                  _isInitialValue = !_isInitialValue;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
